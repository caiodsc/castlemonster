class GameController < ApplicationController

  def create
    game = Game.find_or_create_by(name: params[:name])
    game.assign_attributes(life: 100, monsterLife: 100, specialSpan: 0, cure: 0, round: 0)
    game.save!
    render json: game, status: :ok
  end

  def ranking
    players = Game.where(:date.exists => true).order_by(score: :desc).pluck(:name, :score, :date)
    players.map!{|e| "#{e[0]} - #{e[1].to_i}px - #{e[2].day.to_s.rjust(2, '0')}/#{e[2].month.to_s.rjust(2, '0')}/#{e[2].year}"}
    render json: players, status: :ok
  end

  def play
    game = Game.find_by(name: params[:name])
    render json: {}, status: :method_not_allowed if game.life <= 0 or game.monsterLife <= 0
    game.round += 1
    case params[:type]
    when "ataque"
      game.cure = 0
      game.specialSpan -= 1
      game.monsterAttack = rand(6..12)
      if game.specialSpan == 1
        case rand(1..10)
        when (1..4)
          game.monsterAttack = 0
        end
      else
      end
      game.attack = rand(5..8)
      game.life -= game.monsterAttack
      game.monsterLife -= game.attack
      game.life = 100 if game.life > 100
      game.life = 0 if game.life < 0
      game.specialSpan = 0 if game.specialSpan < 0
      if game.monsterLife < 0
        game.monsterLife = 0
        game.date = Date.today
        score = (game.life * 1000).fdiv(game.round).round(2)
        game.score = score if score > game.score
      end
      game.save!
      render json: game, status: :ok
    when "especial"
      game.cure = 0
      game.attack = rand(7..11)
      game.monsterAttack = rand(6..12)
      case rand(1..4)
      when (1..2)
        game.cure = game.monsterAttack/2
      end
      game.life -= game.monsterAttack
      game.life += game.cure
      game.monsterLife -= game.attack
      game.life = 100 if game.life > 100
      game.life = 0 if game.life < 0
      game.specialSpan = 2
      if game.monsterLife < 0
        game.monsterLife = 0
        game.date = Date.today
        score = (game.life * 1000).fdiv(game.round).round(2)
        game.score = score if score > game.score
      end
      game.save!
      render json: game, status: :ok
    when "cura"
      game.specialSpan -= 1
      game.monsterAttack = rand(6..12)
      if game.specialSpan == 1
        case rand(1..10)
        when (1..4)
          game.monsterAttack = 0
        end
      end
      game.cure = rand(5..10)
      game.life += (game.cure - game.monsterAttack)
      game.life = 100 if game.life > 100
      game.life = 0 if game.life < 0
      game.specialSpan = 0 if game.specialSpan < 0
      game.save!
      render json: game, status: :ok
    else
      render json: game, status: :ok
    end
  end
end
