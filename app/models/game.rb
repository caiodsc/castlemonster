class Game
    include Mongoid::Document
    include Mongoid::Timestamps
    field :name, type: String
    field :life, type: Integer
    field :attack, type: Integer
    field :monsterLife, type: Integer
    field :monsterAttack, type: Integer
    field :cure, type: Integer
    field :round, type: Integer, default: 0
    field :score, type: Float, default: 0
    field :specialSpan, type: Integer
    field :date, type: Date
end