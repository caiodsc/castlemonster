__Dependências__

- __[Rails 5](https://rubyonrails.org/)__ - Framework.

- __[MongoDB](https://www.mongodb.com/download-center)__ - Database.

---

__Comandos__

MongoDB (/bin):
    
    mongod.exe

Rails 5 (Projeto):
    
    bundle install
    
    rails server

---
__Link do Projeto (Back-End):__

- __[CastleMonster](https://castlemonster.herokuapp.com/)__ - https://castlemonster.herokuapp.com. (API)

__Rotas:__

__GET /ranking__

Retorna uma lista com os jogadores, pontuação e data.

__POST /game {name: <Usuário>}__

Cria um novo jogo para o usuário (o recorde deste não é sobrescrito).

__POST /play {name: <Usuário>, type: <Ataque | Especial | Cura>}__

Executa as instruções de ataque/cura e retorna os valores (pontuação, hp, etc).

---